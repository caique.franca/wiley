resource "aws_vpc" "vpc" {
  cidr_block           = var.network.vpn_cidr_block
  enable_dns_hostnames = true

  tags = merge(
    {
      name = "VPC Wiley",
    },
    var.tags,
  )
}

resource "aws_subnet" "subnet_10" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.network.subnet.subnet_10.cidr_block
  map_public_ip_on_launch = true
  availability_zone       = var.network.subnet.subnet_10.availability_zone

  tags = merge(
    {
      name = "Subnet Wiley 10",
    },
    var.tags,
  )
}

resource "aws_subnet" "subnet_11" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.network.subnet.subnet_11.cidr_block
  map_public_ip_on_launch = true
  availability_zone       = var.network.subnet.subnet_11.availability_zone

  tags = merge(
    {
      name = "Subnet Wiley 11",
    },
    var.tags,
  )
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    {
      name = "IGW Wiley",
    },
    var.tags,
  )
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = var.network.rt_cidr_block
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = merge(
    {
      name = "RT Wiley",
    },
    var.tags,
  )
}

resource "aws_route" "rt_default" {
  route_table_id         = aws_route_table.rt.id
  destination_cidr_block = var.network.rt_cidr_block
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "rt_sbn10_association" {
  subnet_id      = aws_subnet.subnet_10.id
  route_table_id = aws_route_table.rt.id
}

resource "aws_route_table_association" "rt_sbn11_association" {
  subnet_id      = aws_subnet.subnet_11.id
  route_table_id = aws_route_table.rt.id
}
