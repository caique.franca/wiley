module "ec2_instance_10" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name                   = "EC2-us-east-2a"
  ami                    = "ami-097a2df4ac947655f"
  instance_type          = "t3.micro"
  key_name               = "wy-b4c14b744c11"
  vpc_security_group_ids = [aws_security_group.sg-ssh.id, aws_security_group.sg-http-alb.id]
  subnet_id              = aws_subnet.subnet_10.id
  user_data              = file("init.sh")

  tags = merge(
    {
      name  = "EC2 Instance10 Wiley",
      sysId = "6f1bb632-da38-4e1f-86c3-6065f8662f88"
      key   = "wy-b4c14b744c11"
    },
    var.tags,
  )
}

resource "aws_eip" "ec2_10" {
  vpc = true

  instance = module.ec2_instance_10.id

  tags = merge(
    {
      name = "Elastic IP",
    },
    var.tags,
  )
}

module "ec2_instance_11" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name                   = "EC2-us-east-2b"
  ami                    = "ami-097a2df4ac947655f"
  instance_type          = "t3.micro"
  key_name               = "wy-b4c14b744c11"
  vpc_security_group_ids = [aws_security_group.sg-ssh.id, aws_security_group.sg-http-alb.id]
  subnet_id              = aws_subnet.subnet_11.id
  user_data              = file("init.sh")

  tags = merge(
    {
      name  = "EC2 Instance11 Wiley",
      sysId = "6f1bb632-da38-4e1f-86c3-6065f8662f88"
      key   = "wy-b4c14b744c11"
    },
    var.tags,
  )
}

resource "aws_eip" "ec2_11" {
  vpc = true

  instance = module.ec2_instance_11.id

  tags = merge(
    {
      Name = "Elastic IP",
    },
    var.tags,
  )
}
