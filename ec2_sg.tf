resource "aws_security_group" "sg-ssh" {
  name        = "access-ssh-sg"
  description = "Allow inbound traffic via SSH"
  vpc_id      = aws_vpc.vpc.id

  ingress = [{
    description      = "SSH from VPC"
    protocol         = "tcp"
    from_port        = 22
    to_port          = 22
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    security_groups  = []
    self             = false

  }]

  egress = [{
    description      = "All traffic"
    protocol         = "-1"
    from_port        = "0"
    to_port          = "0"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    security_groups  = []
    self             = false

  }]

  tags = merge(
    {
      name = "sg_wiley_ssh",
    },
    var.tags,
  )
}

resource "aws_security_group" "sg-http-alb" {
  name        = "access-http-alb-sg"
  description = "Allow inbound HTTP traffic from ALB"
  vpc_id      = aws_vpc.vpc.id

  ingress = [{
    description      = "HTTP from ALB"
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = []
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    security_groups  = [aws_security_group.sg_alb.id]
    self             = false

  }]

  egress = [{
    description      = "All traffic"
    protocol         = "-1"
    from_port        = "0"
    to_port          = "0"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    security_groups  = []
    self             = false

  }]

  tags = merge(
    {
      name = "sg_wiley_ssh",
    },
    var.tags,
  )
}
