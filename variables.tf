variable "network" {
  description = "Network information"
  type = object({
    vpn_cidr_block = string
    subnet = map(object({
      cidr_block        = string
      availability_zone = string
    }))
    rt_cidr_block = string
  })
}

variable "tags" {
  description = "Maps of tags."
  type        = map(any)
  default     = {}
}
