network = {
  vpn_cidr_block = "10.0.0.0/16"
  subnet = {
    subnet_10 = {
      cidr_block        = "10.0.10.0/24"
      availability_zone = "us-east-2a"
    },
    subnet_11 = {
      cidr_block        = "10.0.11.0/24"
      availability_zone = "us-east-2b"
    }
  }
  rt_cidr_block = "0.0.0.0/0"
}

tags = {
  terraform      = "true"
  cloud_provider = "AWS"
  env            = "dev"
  owner          = "Caique Franca"
  owner_email    = "caique.f.queiroz@gmail.com"
}
