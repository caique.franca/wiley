echo -e "$(date)\n" "Updating and installing packages" >> /home/ubuntu/user-data.log
apt-get update

apt-get install software-properties-common -y
apt-get install unzip -y
apt-get install bash-completion -y
apt-get install apt-transport-https -y
apt-get install ca-certificates -y
apt-get install curl -y
apt-get install lsb-release -y
apt-get install linux-headers-$(uname -r) -y
apt-get install build-essential -y

# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg |  apt-key add -

# Use the following command to set up the stable repository
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update

# INSTALL DOCKER ENGINE
echo -e "$(date)\n" "Install Docker" >> /home/ubuntu/user-data.log
apt-get install docker-ce -y
apt-get install docker-ce-cli -y
apt-get install containerd.io -y

# Install Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Docker Compose Autocomplete
curl -L https://raw.githubusercontent.com/docker/compose/1.28.6/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

# Install AWS-CLI
echo -e "$(date)\n" "AWS CLI" >> /home/ubuntu/user-data.log
#mkdir /home/ubuntu/aws-cli/
#curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/home/ubuntu/aws-cli/awscliv2.zip" && unzip -o /home/ubuntu/aws-cli/awscliv2.zip && /home/ubuntu/aws-cli/aws/install
apt-get install awscli -y