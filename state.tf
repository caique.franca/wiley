# Terraform backend for remote state
terraform {
  backend "s3" {
    encrypt = true
    bucket  = "wy-b4c14b744c11"
    #dynamodb_table          = ""
    region = "us-east-2"
    key    = "terraform.tfstate"
  }
}
