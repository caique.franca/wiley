
resource "aws_security_group" "sg_alb" {
  name        = "SG ALB Wiley"
  description = "Terraform load balancer security group"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    {
      name = "SG ALB Wiley",
    },
    var.tags,
  )
}
resource "aws_lb" "alb" {
  name               = "ALB-Wiley"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg_alb.id]
  subnets            = [aws_subnet.subnet_10.id, aws_subnet.subnet_11.id]

  enable_deletion_protection = false

  tags = merge(
    {
      name = "ALB Wiley",
    },
    var.tags,
  )
}

resource "aws_lb_target_group" "tg_alb" {
  name     = "TG-ALB-Wiley"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id

  health_check {
    path = "/test.php"
    port = 80
  }
}

resource "aws_lb_listener" "listener_http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.tg_alb.arn
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "tg_alb_attachment_ec2_10" {
  target_group_arn = aws_lb_target_group.tg_alb.arn
  target_id        = module.ec2_instance_10.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "tg_alb_attachment_ec2_11" {
  target_group_arn = aws_lb_target_group.tg_alb.arn
  target_id        = module.ec2_instance_11.id
  port             = 80
}
