output "instance_id_10" {
  description = "The ID of the instance 10."
  value       = module.ec2_instance_10.id
}

output "instance_id_11" {
  description = "The ID of the instance 11."
  value       = module.ec2_instance_11.id
}

output "dns_name" {
  description = "The DNS name of the load balancer."
  value       = aws_lb.alb.dns_name
}

output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.vpc.id
}

output "subnet_id_10" {
  description = "The ID of the Subnet"
  value       = aws_subnet.subnet_10.id
}

output "subnet_id_11" {
  description = "The ID of the Subnet"
  value       = aws_subnet.subnet_11.id
}
